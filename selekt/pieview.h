#ifndef PIEVIEW_H
#define PIEVIEW_H

#include <QWidget>

class PieView 
	: public QWidget
{
	Q_OBJECT

	public:
		PieView(QWidget* parent = 0);
		~PieView();
		void setValues ( int _ok, int _nok, int _nsnc, int _all);
		
		virtual QSize sizeHint() const;
	protected:
		void paintEvent(QPaintEvent* );
		void mouseMoveEvent(QMouseEvent *event);
		void focusOutEvent(QFocusEvent* );
		
	private:
		QPointF calculateTranslation (int angle);
		void setCurrentAngle(int angle);
		
		enum Sections { Ok, No, NsNc, All };
		QVector<QColor> m_colors;
		QVector<float> m_spannings;
		QVector<int> m_values;
		
		int currentAngle;
// 		int radi;
		bool inside;
		bool notMove;
		int m_all;
};

 #endif