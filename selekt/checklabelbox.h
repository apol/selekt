/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef CHECKLABELBOX_H
#define CHECKLABELBOX_H

#include <QCheckBox>
#include <QHBoxLayout>
#include <QMouseEvent>
#include <QString>
#include <QTextEdit>

class TextEdit : public QTextEdit 
{
	Q_OBJECT
	signals:
		void leftClick();
	private:
		void mousePressEvent(QMouseEvent* event)
		{
			if (event->button() == Qt::LeftButton) emit leftClick();
		}
};

class CheckLabelBox : public QWidget
{
	Q_OBJECT
	public:
		CheckLabelBox ( QString letter, QWidget* parent = 0);
		~CheckLabelBox ();
		void setText(QString text);
		void setChecked(bool check);
		bool checkState();
		
	protected:
		void mousePressEvent(QMouseEvent* event);

	private slots:
		void emitLeft();
		void stateChanged();
	private:
		QHBoxLayout* mainLayout;
		QCheckBox* cb;
		TextEdit* te;
	signals:
		void leftClick();
};

#endif // CHECKLABELBOX_H
