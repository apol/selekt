#include "abstractimporter.h"
#include "importerfactory.h"
#include "selekt.h"
#include "questionview.h"
#include "statistic.h"
#include "extract.h"

#include <KAction>
#include <KActionCollection>
#include <KApplication>
#include <KFileDialog>
#include <KLocale>
#include <KMessageBox>
#include <KStatusBar>
#include <KXmlGuiWindow>

#include <QAbstractItemModel>
#include <QDebug>
#include <QDockWidget>
#include <QListView>
#include <QString>
#include <QTime>

Selekt::Selekt ( QWidget* parent )
		: KXmlGuiWindow ( parent )
{
	setMinimumSize (900, 600);

	m_questionView = new QuestionView ( this );
	setCentralWidget ( m_questionView );

	QDockWidget* dock = new QDockWidget (i18n ("Questions"), this);
	dock->setAllowedAreas(Qt::AllDockWidgetAreas);
	dock->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);
	addDockWidget (Qt::RightDockWidgetArea, dock);

	m_listView = new QListView (dock);
	m_listView->setHorizontalScrollBarPolicy (Qt::ScrollBarAlwaysOff);
	m_listView->setAlternatingRowColors (true);
	m_listView->setTextElideMode (Qt::ElideRight);
	dock->setWidget (m_listView);

	setMenu();

	connect ( m_listView, SIGNAL ( activated (QModelIndex) ), this, SLOT ( slotUpdateGUI(QModelIndex)) );

	timeElapsed = new QTime(0,0,0,0);
	isLoaded = false;
	oldPath.clear();
}

Selekt::~Selekt() {
	Extract ext(this);
	ext.removeAllExisting();
}

void Selekt::setMenu() {
	// File entries
	KStandardAction::open (this, SLOT (slotLoadFile()), actionCollection());
	KStandardAction::quit(kapp, SLOT(quit()), actionCollection());

	// Question entries
	nextQuestion = new KAction(i18n("Next question"), this);
	actionCollection()->addAction("next_question", nextQuestion);
	nextQuestion->setIcon(KIcon("go-down"));
	nextQuestion->setEnabled(false);
	connect(nextQuestion, SIGNAL(triggered(bool)), this, SLOT(slotNextQuestion()));

	previousQuestion = new KAction(i18n("Previous question"), this);
	actionCollection()->addAction("previous_question", previousQuestion);
	previousQuestion->setIcon(KIcon("go-up"));
	previousQuestion->setEnabled(false);
	connect(previousQuestion, SIGNAL(triggered(bool)), this, SLOT(slotPreviousQuestion()));

	result = new KAction(i18n("Result"), this);
	actionCollection()->addAction("result", result);
	result->setIcon(KIcon("games-endturn"));
	result->setEnabled(false);
	connect(result, SIGNAL(triggered(bool)), this, SLOT(slotResult()));

	setupGUI(Default, "selekt.rc");
}

void Selekt::slotLoadFile() {
	bool isFileValid = true;
	QString extensions;
	QString allExtensions;
	QStringList supportedExtensions = ImporterFactory::self()->extensions();
	QStringList asteriskDotExtension;
	foreach ( const QString& ext, supportedExtensions ) {
		extensions += QString ( "\n*.%1|*.%1" ).arg ( ext );
		allExtensions += QString ( "*.%1 " ).arg ( ext );
		asteriskDotExtension << "*." + ext;
	}
	extensions += QString ("\n*.tar.gz|*.tar.gz");
	allExtensions += QString ("*.tar.gz ");

	QString location = KFileDialog::getOpenFileName (KUrl(), allExtensions + i18n("|All Supported Files") + extensions);
	if ( !location.isEmpty() ) {
		QFileInfo file(location);
		QString extensio = file.completeSuffix();

		if (extensio.endsWith("tar.gz")) {
			Extract ex(this);
			location = ex.extractFile(location, asteriskDotExtension);
			if (location.isEmpty()) {
				//Error
				qDebug()<<"There's no valid file";
				isFileValid = false;
			}
			else {
				file.setFile(location);
				extensio = file.completeSuffix();
			}
		}

		if (isFileValid) {
			if (isLoaded) {
				delete model;
				//TODO: check if user wants to see the result
			}

			AbstractImporter* importer = ImporterFactory::self()->get ( extensio );
			model = importer->import ( location, m_listView);
			m_listView->setModel (model);
			m_listView->setEditTriggers (QAbstractItemView::NoEditTriggers);
			m_questionView->setModel (model);
			m_questionView->clearGUI();
			slotUpdateGUI (model->index (0, 0));
			statusBar()->showMessage (i18n ("Loaded %1").arg (location), 2000);
			isLoaded = true;
			timeElapsed->restart();
			result->setEnabled(true);
			nextQuestion->setEnabled(true);
			previousQuestion->setEnabled(true);
		}
	}
}

void Selekt::slotNextQuestion() {
	if (isLoaded) {
		int row = actualItem.row () + 1;
		if (model->rowCount() == row)
			row = 0;
		slotUpdateGUI(model->index(row, 0));
	}
}

void Selekt::slotPreviousQuestion() {
	if (isLoaded) {
		int row = actualItem.row () - 1;
		if (row < 0)
			row = model->rowCount()-1;
		slotUpdateGUI(model->index (row, 0));
	}
}

void Selekt::slotUpdateGUI ( const QModelIndex& item ) {
	actualItem = item;
	m_questionView->loadQuestion ( item );
	m_listView->setCurrentIndex(model->index(item.row(),0));
}

void Selekt::slotResult() {
	if ( isLoaded ) {
		m_questionView->storeLastItem();
		int ok=0;
		int nsnc = 0;
		int nok = 0;

		statusBar()->showMessage ( i18n ( "Taking the result..." ), 1000 );
		int nQuests = model->rowCount();
		for ( int i=0 ; i< nQuests ; ++i ) {
			QModelIndex tmp = model->index ( i,0 );
			int answerNumber = model->rowCount ( tmp );
			bool good = false;
			bool notGood = false;
			bool notAnswered = true;
			for ( int j=0 ; j < answerNumber ; ++j ) {
				QVariant isChecked = model->data ( tmp.child ( j,0 ),Qt::CheckStateRole );
				QVariant isOk = model->data ( tmp.child ( j,0 ),Qt::UserRole );
				if (isChecked=="check") {
					notAnswered = false;
					if (isOk == "True")
						good = true;
					if (isOk != "True")
						notGood = true;
				}
				else {
					if (isOk == "True")
						notGood = true;
				}
			}
			if ( notGood ) {
				if (notAnswered)
					nsnc++;
				else
					nok++;
			}
			else if (good)
				ok++;
			else // if (!notAnswered)
				nsnc++;
		}
		qDebug()<< "Ok: "<< ok <<" - NOk: " << nok <<" - Ns/Nc: "<< nsnc;

		Statistic* s = new Statistic ( this );
		s->show();
		s->showTimeElapsed(timeElapsed->elapsed());
		s->addData ( ok, nok, nsnc, model->rowCount() );

		m_questionView->clearGUI();
		delete model;
		isLoaded = false;
		result->setEnabled(false);
		previousQuestion->setEnabled(false);
		nextQuestion->setEnabled(false);
	}
	else
		QMessageBox::critical (this,i18n ("Error"),i18n("There is no opened file"));
}

