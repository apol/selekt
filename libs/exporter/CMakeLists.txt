set(selektExporterData_SRCS
	abstractexporter.cpp
	exporterfactory.cpp
	txtexporter.cpp
	xmlexporter.cpp
	../extract.cpp)

kde4_add_library(selektExporterData SHARED ${selektExporterData_SRCS})

target_link_libraries(selektExporterData ${KDE4_KIO_LIBRARY} ${QT_QTXML_LIBRARY})

install(TARGETS selektExporterData ${INSTALL_TARGETS_DEFAULT_ARGS})
