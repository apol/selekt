/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef LISTEDITOR_H
#define LISTEDITOR_H

#include "editablemodel.h"

#include <QListView>
#include <QStandardItemModel>

class ListEditor : public QWidget
{
	Q_OBJECT
	public:
		ListEditor(QWidget* parent=0);
		~ListEditor();
		void setModel(EditableModel* _model);
	signals:
		void activated (const QModelIndex& item, const bool setUpdate);
	public slots:
		void setCurrentIndex(const QModelIndex & index);
// 		m_listView->setCurrentIndex(model->index(item.row(),0));
	private slots:
		void emitSignal(const QModelIndex& item, const bool saveData);
		void emitNormalSignal(const QModelIndex& item);
		void addQuestion();
		void delQuestion();
	private:
		EditableModel* m_model;
		QListView* m_listView;
		QModelIndex actualItem;
};

#endif // LISTEDITOR_H
