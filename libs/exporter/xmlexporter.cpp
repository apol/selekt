/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "exporterfactory.h"
#include "xmlexporter.h"

#include <QAbstractItemModel>
#include <QDebug>
#include <QFile>

REGISTER_EXPORTER("kst", XmlExporter)

bool XmlExporter::exporter ( const QAbstractItemModel& model, const QString& path )
{
	QFile file(path);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return false;
	
	QTextStream out(&file);
	out << "<document>\n";
	
	const int m_rows = model.rowCount();
	for (int i=0 ; i < m_rows ; ++i)
	{
		QModelIndex idx = model.index(i,0);
		out << "\t<question>\n";
		out << "\t\t<statement>"<< idx.data().toString() << "</statement>\n";
		
		const int m_ans = model.rowCount(idx);
		for (int ans=0 ; ans < m_ans ; ++ans)
		{
			QModelIndex ansIdx = model.index(ans, 0, idx);
			if (ansIdx.data(Qt::UserRole) == "True")
				out << "\t\t<answer correct=\"1\">";
			else
				out << "\t\t<answer correct=\"0\">";
			out << ansIdx.data().toString() << "</answer>\n";
		}
		out << "\t</question>\n";
	}
	out << "</document>\n";

	file.close();
	
	return true;
}

