#include "editablemodel.h"

#include <QDebug>
#include <QStandardItem>
#include <QStandardItemModel>

EditableModel::EditableModel ( const QAbstractItemModel* model, QObject* parent)
	: QStandardItemModel(parent)
{
	for(int i = 0 ; i < model->rowCount() ; ++i) {
		QModelIndex qidx = model->index(i, 0);
		QStandardItem* question = new QStandardItem(qidx.data().toString());
		question->setData(qidx.data(Qt::UserRole), Qt::UserRole);
		
		for(int ans=0 ; ans < model->rowCount(qidx) ; ++ans) {
			QModelIndex aidx = model->index(ans, 0, qidx);
			QStandardItem* answer = new QStandardItem(aidx.data().toString());
			if (aidx.data(Qt::UserRole)=="True")
				answer->setData("True",Qt::UserRole);
			question->appendRow(answer);
		}
		appendRow(question);
	}
}

EditableModel::~EditableModel() {}

void EditableModel::appendQuestion ()
{
	QStandardItem* aItem = new QStandardItem (i18n("Empty statement"));
	appendRow(aItem);

	//By default creates four possibles answers
	for (int i=0 ; i < 4 ; ++i)
	{
		QStandardItem* ans = new QStandardItem (i18n ("Empty answer"));
		aItem->appendRow (ans);
	}
}

void EditableModel::deleteQuestion ( const QModelIndex& questionIdx )
{
	if (rowCount () > 1)
		removeRow (questionIdx.row());
}

void EditableModel::appendAnswer(const QModelIndex& questionIdx)
{
	QStandardItem* question=itemFromIndex(questionIdx);
	question->appendRow(new QStandardItem(i18n ("Empty answer...")));
}

void EditableModel::deleteAnswer (const QModelIndex& questionIdx )
{
	//By default always exists at least 2 possible answers
	QStandardItem* aItem = itemFromIndex(questionIdx);
	if (aItem->rowCount() > 2)
	{
		aItem->removeRow(aItem->rowCount()-1);
	}
}

void EditableModel::setQuestionData ( const QModelIndex& index, const QVariant& value, int role)
{
	setData(index, value.toString(), role);
}

