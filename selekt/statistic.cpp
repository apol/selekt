/*
    Copyright (C) 2009 Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "statistic.h"
#include "pieview.h"

#include <QDebug>
#include <QLabel>
#include <QPainter>
#include <QRectF>
#include <QString>
#include <QTime>
#include <QVBoxLayout>
#include <QWidget>

#include <KGlobal>
#include <KLocale>
#include <KMainWindow>
#include <KPushButton>

Statistic::Statistic (QWidget* parent):
	KDialog(parent)
{}

Statistic::~Statistic() {}

void Statistic::addData(int _ok, int _nok, int _nsnc, int _all) 
{
	QWidget* w = new QWidget(this);
	setMainWidget(w);
	
	QLabel* lOk = new QLabel(i18n("There are %1 correct answers", _ok), w);
	QLabel* lNok = new QLabel(i18n("There are %1 incorrect answers", _nok), w);
	QLabel* lNsnc = new QLabel(i18n("There are %1 no answered questions", _nsnc), w);
	QLabel* lTimeElapsed = new QLabel(i18n("Time elapsed: %1", KGlobal::locale()->formatTime(timeElapsed,true)), w);
	
	PieView* pie = new PieView(this);
	pie->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
	pie->setValues(_ok, _nok, _nsnc, _all);
	
	QVBoxLayout* layout = new QVBoxLayout(w);
	layout->addWidget(pie);
	layout->addWidget(lOk);
	layout->addWidget(lNok);
	layout->addWidget(lNsnc);
	layout->addWidget(lTimeElapsed);
	
	setButtons(Close);
}


void Statistic::showTimeElapsed ( int elapsed )
{
	int segundo = elapsed / 1000;
	int minuto = segundo / 60;
	segundo = segundo % 60;
	int hora = minuto / 60;
	minuto = minuto % 60;

	timeElapsed.setHMS(hora,minuto,segundo,elapsed%1000);
}

