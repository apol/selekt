/*
    Copyright (C) 2009  Victor Blazquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef EXTRACT_H
#define EXTRACT_H

#include <kdemacros.h>
#include <QWidget>

class QString;
class QStringList;

class KDE_EXPORT Extract : public QWidget
{
	Q_OBJECT
	public:
		Extract(QWidget* parent =0) ;
		~Extract();
		QString extractFile(QString file, QStringList allSupportedExtensions);
		void removeAllExisting();
};

#endif // EXTRACT_H
