/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef EXPORTERFACTORY_H
#define EXPORTERFACTORY_H

#include <kdemacros.h>

#include <QMap>
#include <QString>
#include <QStringList>

class AbstractExporter;
class QObject;

#define REGISTER_EXPORTER(ext, classname) \
	AbstractExporter* create##classname () { return new classname; } \
	bool b##classname = ExporterFactory::self()->registerExporter(ext, create##classname);

class KDE_EXPORT ExporterFactory
{
	public:
		typedef AbstractExporter* (*ExporterCreator)();
		typedef QString Key;
		static ExporterFactory* self();
		QStringList extensions() const;
		
		bool registerExporter(const Key& key, ExporterCreator);
		AbstractExporter* get(const Key& key) const;
	private:
		ExporterFactory();
		static ExporterFactory* m_self;
		
		QMap<Key, ExporterCreator> m_exporters;
};

#endif // EXPORTERFACTORY_H
