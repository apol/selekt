#include "checklabelbox.h"

#include <QDebug>
#include <QHBoxLayout>
#include <QLabel>
#include <QString>
#include <QTextEdit>

#include <KLocale>
#include <KPushButton>

CheckLabelBox::CheckLabelBox ( QString letter, QWidget* parent ) 
	: QWidget ( parent )
{
	cb = new QLabel;
	cb->setText(letter);
	QVBoxLayout* possibleAnswers = new QVBoxLayout;
	
	addOk = new KPushButton;
	addOk->setIcon(KIcon("dialog-ok-apply"));
	addFalse = new KPushButton;
	addFalse->setIcon(KIcon("edit-delete"));
	possibleAnswers->addWidget(addOk);
	possibleAnswers->addWidget(addFalse);

	te = new QTextEdit;
	te->setAcceptRichText(true);
	te->setMinimumHeight(50);
	mainLayout = new QHBoxLayout(this);
	mainLayout->addWidget(cb);
	mainLayout->addLayout(possibleAnswers);
	mainLayout->addWidget(te);

	connect(addOk, SIGNAL(clicked(bool)),this,SLOT(setOk()));
	connect(addFalse, SIGNAL(clicked(bool)),this,SLOT(setFalse()));
	setAutoFillBackground(true);
}

CheckLabelBox::~CheckLabelBox() {}

void CheckLabelBox::setText ( QString text )
{
	te->setText(text);
}

QString CheckLabelBox::text ()
{
	return te->toPlainText();
}

void CheckLabelBox::setOk()
{
	QPalette p = QWidget::palette();
	p.setColor(QPalette::All , QPalette::Window, Qt::green);
	setPalette(p);
	addOk->setEnabled(false);
	addFalse->setEnabled(true);
}

void CheckLabelBox::setFalse()
{
	QPalette palette = this->palette();
	palette.setColor(QPalette::All, QPalette::Window, Qt::red);
	setPalette(palette);
	addFalse->setEnabled(false);
	addOk->setEnabled(true);
}


QString CheckLabelBox::checkState()
{
	if (addOk->isEnabled())
		return "False";
	else
		return "True";
}
