/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GIFTIMPORTER_H
#define GIFTIMPORTER_H

#include "abstractimporter.h"
#include "giftparser.h"

class QStandardItem;
class QAbstractItemModel;
class GiftVisitor : public AbstractGiftVisitor
{
public:
	GiftVisitor();

    virtual void titleFound(const QString& title);
    virtual void textFound(const QString& text);
    virtual void commentFound(const QString& comment) {}
    virtual void error(const QString& error);

    virtual void openQuestion();
    virtual void openAnswer(bool numerical);
    virtual void closeAnswer();
    virtual void closeQuestion();
    
    virtual void booleanAnswer(bool answer);
    virtual void matchAnswer(const QString& from, const QString& to, const QString& feedback);
//     virtual void numericalAnswer(const QString& value, int percentage, const QString& range, const QString& feedback);
	void numericalAnswer(bool interval, const QString& value, int percentage, const QString& range, const QString& feedback)	;
    virtual void testAnswer(bool correct, int percentage, const QString& text, const QString& feedback);


	QStandardItemModel* model;
	QStandardItem* current;
	QStandardItem* currentQuestion;
};

class KDE_EXPORT GiftImporter : public AbstractImporter
{
	public:
		virtual QAbstractItemModel* import(const QString& path, QObject* parent = 0);
};

#endif // GIFTIMPORTER_H


