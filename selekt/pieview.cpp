#include "pieview.h"

#include <QDebug>
#include <QMouseEvent>
#include <QPainter>

// #include <KGlobal>
#include <KLocale>

#include <cmath>

PieView::PieView(QWidget *parent)
	: QWidget(parent), currentAngle(0), notMove(false), m_all(0)
{
	setBackgroundRole(QPalette::AlternateBase);
	setAutoFillBackground(true);
	setMouseTracking(true);
	
	m_colors.resize(All);
	m_spannings.resize(All);
	m_values.resize(All);
	m_colors[Ok]=Qt::green;
	m_colors[No]=Qt::red;
	m_colors[NsNc]=Qt::gray;
}

PieView::~PieView() {}

void PieView::setValues ( int ok, int nok, int nsnc, int all )
{
	//TODO: pass a qlist as parameter, move to a loop
	//Be careful with integer roundings
	
	int aa = ok+nok+nsnc;
	m_spannings[Ok]=360*ok/aa;
	m_spannings[No]=360*nok/aa;
	m_spannings[NsNc]=360*nsnc/aa;
	
	m_values[Ok] = ok;
	m_values[No] = nok;
	m_values[NsNc] = nsnc;
	m_all=all;
	
	if (ok == all || nok == all || nsnc == all)
		notMove = true;
	
	repaint();
}

void PieView::paintEvent(QPaintEvent* )
{
	int side;
	QPoint topLeft=rect().topLeft();
	if(rect().width()>rect().height()) {
		side=rect().height();
		int diff=rect().width()-rect().height();
		topLeft.rx() += diff/2;
	} else {
		side=rect().width();
		int diff=rect().height()-rect().width();
		topLeft.ry() += diff/2;
	}
	side -= 40;
	topLeft.ry() += 20;
	topLeft.rx() += 20;
	
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing);
	if (!notMove)
		painter.setPen(QPen(palette().base(), 0));
	painter.setBrush(palette().base());
	painter.drawRect(0,0,width(),height());
	QRectF rectangle(topLeft, QSize(side,side));

	int startAngle=0;
	for(int i=0; i<All; ++i) {
		painter.save();
		int spanAngle = m_spannings[i];
		painter.setBrush(m_colors[i]);
		
		if (currentAngle>startAngle && currentAngle<(startAngle+spanAngle) ) {
			if (!notMove)
			{
				QPointF vec = calculateTranslation(startAngle+spanAngle/2);
				painter.translate( vec );
			}
			double percent = (double)m_values[i]*100/m_all;
			QString txt;
			if (i == Ok) txt = i18n("Ok");
			if (i == No) txt = i18n("Wrong");
			if (i == NsNc) txt = i18n("Ns/Nc");
// 			setToolTip(i18n("%1\% %2", KGlobal::locale()->formatNumber(percent,2), txt));
			setToolTip(ki18n("%1% %2").subs(percent, 0, 'f', 2).toString().arg(txt));
		}
		if (i+1==All)
			painter.drawPie(rectangle, startAngle*16, 360*16-(startAngle*16));
		else
			painter.drawPie(rectangle, startAngle*16, spanAngle*16);
		painter.restore();
		startAngle+=spanAngle;
	}
}

QSize PieView::sizeHint() const
{
    return QSize(300, 250);
}

void PieView::mouseMoveEvent(QMouseEvent *event)
{
	QPoint p = event->pos();
	QPoint vec = p-rect().center();

	double radi;
	if (width()< height()) 
		radi = (width()-40) /2;
	else
		radi = (height()-40) /2;
	
	if ( (pow(p.rx()-rect().center().rx(),2) + pow(p.ry()-rect().center().ry(),2) ) <= pow(radi,2) )
	{
		int aa = int(std::atan2(vec.x(), vec.y())*180/M_PI)+180;
		setCurrentAngle((aa+90)%360);
	}
	else
		setCurrentAngle(-1);
}

QPointF PieView::calculateTranslation(int bisect)
{
	double rad=bisect*M_PI/180;
	double x=std::cos(rad);
	double y=std::sin(rad);
	
	const int scalar=20;//the translation vector size
	return QPointF(x,-y)*scalar;
}

void PieView::focusOutEvent(QFocusEvent* ev)
{
	QWidget::focusOutEvent(ev);
	setCurrentAngle(-1);
}

void PieView::setCurrentAngle(int angle)
{
	currentAngle=angle;
	repaint();
}
