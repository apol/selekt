/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QUESTIONVIEW_H
#define QUESTIONVIEW_H

#include "checklabelbox.h"
#include "editablemodel.h"

#include <QGroupBox>
#include <QList>
#include <QModelIndex>
#include <QSplitter>
#include <QStandardItemModel>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QWidget>

class QuestionView : public QWidget
{
	Q_OBJECT
	public:
		QuestionView (QWidget* parent );
		~QuestionView();
		void loadQuestion ( const QModelIndex& item, bool saveData = true);
		void setModel ( EditableModel* im );
		void storeLastItem();
		void clearGUI();
		void clearQuestion();
	private slots:
		void addAnswer();
		void delAnswer();

	private:
		void saveActualData();

		EditableModel* model;
		QModelIndex actualItem;
		QModelIndex* oldItem;
		QTextEdit* statement;
		QGroupBox* radioGroup;
		QVBoxLayout* radioL;
		QList<CheckLabelBox* >* listCheckBoxes;
		bool loaded;
		QSplitter* splitMain;
		QVBoxLayout* radioLayout;
		QWidget* widgetDownSplit;
};

#endif // QUESTIONVIEW_H
