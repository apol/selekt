#ifndef GIFTPARSERTEST_H
#define GIFTPARSERTEST_H

#include <QtTest/QTest>

class GiftParserTest : public QObject
{
	Q_OBJECT
	private slots:
		void giftTest();
		void giftTest_data();
};

#endif // GIFTPARSERTEST_H
