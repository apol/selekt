/*
    Copyright (C) 2009  Víctor Blázque Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "exporterfactory.h"

ExporterFactory* ExporterFactory::m_self=0;

ExporterFactory::ExporterFactory() {}

ExporterFactory* ExporterFactory::self()
{
	if(!m_self)
		m_self = new ExporterFactory;
	return m_self;
}

AbstractExporter* ExporterFactory::get(const ExporterFactory::Key& key) const
{
	Q_ASSERT(m_exporters.contains(key));
	return m_exporters[key]();
}

bool ExporterFactory::registerExporter(const Key& key, ExporterCreator c)
{
	m_exporters[key]=c;
	return true;
}

QStringList ExporterFactory::extensions() const
{
	return m_exporters.keys();
}
