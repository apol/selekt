#include "extract.h"

#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QProcess>
#include <QStringList>
#include <QWidget>

#include <KIO/NetAccess>

Extract::Extract(QWidget* parent)
	: QWidget ( parent )
{}

Extract::~Extract() {}

QString Extract::extractFile ( QString file, QStringList allSupportedExtensions )
{
	QString oldPath = QDir::currentPath();
	QStringList arguments;
	arguments << "-zxf" << file;
	QDateTime dt  = QDateTime::currentDateTime();
	QString appTmp = QDir::tempPath() + "/selekt/" + dt.toString("HHmmsszzz");
	QDir directory;

	if (!directory.mkpath(appTmp)) {
		qDebug()<< "Directory not created";
		if (!directory.exists(appTmp)) {
			qDebug()<< "Directory not exists" << appTmp;
			return 0;
		}
	}
	
	if (!QDir::setCurrent(appTmp)) {
		qDebug()<< "Cannot change the path";
		return 0;
	}
	
	QProcess extract;
	extract.start("tar", arguments);
	if (!extract.waitForStarted())
		return 0;
	
	extract.closeWriteChannel();
	if (!extract.waitForFinished())
		return 0;
	
	directory.setPath(appTmp);
	QStringList fileList = directory.entryList(allSupportedExtensions);
	if (fileList.size() == 0) 
	{
		qDebug() << "There isn't any valid file";
		return 0;
	}
	
	QDir::setCurrent(oldPath);
	return appTmp + "/" + fileList.at(0);
}

void Extract::removeAllExisting()
{
	QString pathTmp = QDir::tempPath() + "/selekt";
	
	QDir dirTmp(pathTmp);
	if (dirTmp.exists()) {
		KUrl url(pathTmp);
			if ( !KIO::NetAccess::del( url, this ) ) {
				qDebug()<< "Cannot delete: "<<  url.pathOrUrl() ;
			}
		}
}



