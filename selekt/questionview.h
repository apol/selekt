/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QUESTIONVIEW_H
#define QUESTIONVIEW_H

#include "checklabelbox.h"

#include <QCheckBox>
#include <QGroupBox>
#include <QList>
#include <QModelIndex>
#include <QSplitter>
#include <QVBoxLayout>
#include <QWidget>

#include <KHTMLPart>

class QuestionView : public QWidget
{
	Q_OBJECT
	public:
		QuestionView (QWidget* parent );
		~QuestionView() {};
		void loadQuestion ( const QModelIndex& item );
		void setModel ( QAbstractItemModel* im );
		void storeLastItem();
		void clearModel();
		void clearGUI();
	private:
		void checkState();	
		
		QAbstractItemModel* model;
		KHTMLPart* statement;
		QGroupBox* radioGroup;
		QVBoxLayout* radioL;
		QList<CheckLabelBox* >* lcb;
		QModelIndex* oldItem;
		bool loaded;
		QSplitter* splitMain;
};

#endif // QUESTIONVIEW_H
