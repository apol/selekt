/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "checklabelbox.h"

#include <QDebug>
#include <QHBoxLayout>
#include <QString>

#include <KLocale>

CheckLabelBox::CheckLabelBox ( QString letter, QWidget* parent )
	: QWidget ( parent )
{
	cb = new QCheckBox;
	cb->setText(letter);
	te = new TextEdit;
	te->setReadOnly(true);
	te->setAcceptRichText(true);
	te->setMinimumHeight(50);
	mainLayout = new QHBoxLayout(this);
	mainLayout->addWidget(cb);
	mainLayout->addWidget(te);

	connect(te,SIGNAL(leftClick()),this,SLOT(emitLeft()));
	connect(cb,SIGNAL(stateChanged(int)),this,SLOT(stateChanged()));
	setAutoFillBackground(true);
}

CheckLabelBox::~CheckLabelBox() {}


void CheckLabelBox::setText ( QString text )
{
	te->setText(text);
}

void CheckLabelBox::setChecked ( bool check )
{
	cb->setChecked(check);
	if (cb->isChecked())
		setBackgroundRole(QPalette::Dark);
	else
		setBackgroundRole(QPalette::Window);
}

bool CheckLabelBox::checkState()
{
	return cb->checkState();
}

void CheckLabelBox::mousePressEvent(QMouseEvent* event)
{
	if(event->button()==Qt::LeftButton)
		emitLeft();
}

void CheckLabelBox::emitLeft()
{
	if (cb->isChecked())
	{
		cb->setChecked(false);
		setBackgroundRole(QPalette::Window);
	}
	else
	{
		cb->setChecked(true);
		setBackgroundRole(QPalette::Dark);
	}
}

void CheckLabelBox::stateChanged()
{
	if (cb->isChecked())
		setBackgroundRole(QPalette::Dark);
	else
		setBackgroundRole(QPalette::Window);
}

