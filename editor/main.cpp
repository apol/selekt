#include "selekteditor.h"

#include <KApplication>
#include <KAboutData>
#include <KCmdLineArgs>
#include <KMessageBox>


int main(int argc, char** argv)
{
	KAboutData aboutData("selekt-editor", 0, ki18n("Selekt Editor"), "0.2",
			ki18n("Program for tests"),
			KAboutData::License_GPL, ki18n("(c) 2009"),
			ki18n("Application to make tests"),
			"",
			"victor.blazquez@gmail.com");
	aboutData.addAuthor( ki18n("Víctor Blázquez Francisco"), KLocalizedString(), "victor.blazquez@gmail.com" );
	
	KCmdLineArgs::init(argc, argv, &aboutData);
	KApplication app;
	SelektEditor *t = new SelektEditor;
	t->show();
	return app.exec();
}
