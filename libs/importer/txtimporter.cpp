/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "importerfactory.h"
#include "txtimporter.h"

#include <QDebug>
#include <QFile>
#include <QStandardItemModel>

#include <KLocale>

REGISTER_IMPORTER("txt", TxtImporter)

QAbstractItemModel* TxtImporter::import(const QString& path, QObject* parent)
{
	QStandardItemModel* m = new QStandardItemModel(parent);

	QFile file(path);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		qDebug()<< i18n("Cannot read .txt file");
	}

	QTextStream in (&file);
	QString line;
	int i=0;
	QStandardItem* ultima = 0;

	while (!in.atEnd())
	{
		line = in.readLine();
		if (line.startsWith("-",Qt::CaseInsensitive))
		{
			QString lineTmp = line.right(line.size()-1);
			QStandardItem* aItem = new QStandardItem(lineTmp);
			ultima = aItem;
			m->appendRow(ultima);
		}
		else
		{
			QStandardItem* aItem;
			if (line.startsWith('*',Qt::CaseInsensitive))
			{
				QString lineTmp= line.right(line.size()-1);
				aItem = new QStandardItem(lineTmp);
				aItem->setData("True",Qt::UserRole);
			}
			else
			{
				aItem = new QStandardItem(line);
				aItem->setData("False",Qt::UserRole);
			}

			ultima->appendRow(aItem);
		}
		++i;
	}

	file.close();

	return m;
}

