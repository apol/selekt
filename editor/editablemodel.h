/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef EDITABLEMODEL_H
#define EDITABLEMODEL_H

#include <QStandardItemModel>

#include <KLocale>

class EditableModel : public QStandardItemModel
{
	Q_OBJECT
	public:
		EditableModel ( const QAbstractItemModel* model, QObject* parent = 0 );
		~EditableModel();
		void appendQuestion ();
		void deleteQuestion (const QModelIndex& questionIdx);
		void appendAnswer (const QModelIndex& questionIdx);
		void deleteAnswer (const QModelIndex& questionIdx);
		void setQuestionData (const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
	private:

};

#endif // EDITABLEMODEL_H
