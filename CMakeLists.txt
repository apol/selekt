project(selekt)

find_package(KDE4 REQUIRED)
include_directories(${KDE4_INCLUDES})
# add_definitions("-Wall -ansi -pedantic")

add_subdirectory(icons)
add_subdirectory(libs)
add_subdirectory(selekt)
add_subdirectory(editor)
