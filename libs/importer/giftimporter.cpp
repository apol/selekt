/*
    Copyright (C) 2009  Víctor Blázquez Francisco <victor.blazquez@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "importerfactory.h"
#include "giftimporter.h"
#include "giftparser.h"

#include <QDebug>
#include <QFile>
#include <QStandardItemModel>

#include <KLocale>

REGISTER_IMPORTER("gift", GiftImporter)

QAbstractItemModel* GiftImporter::import(const QString& path, QObject* parent)
{
	QFile file(path);
	file.open(QFile::ReadOnly);
	QTextStream f(&file);

	GiftVisitor visitor;
	GiftParser p(&f);
	p.start(&visitor);
	return visitor.model;
}

GiftVisitor::GiftVisitor()
{
	model = new QStandardItemModel;
	current = new QStandardItem;
}

void GiftVisitor::titleFound(const QString& title)
{
// 	if(current && !current->text().isEmpty())
// 		model->appendRow(current);

// 	current = new QStandardItem(QString("<b>%1</b> ").arg(title.trimmed()));
	current->setText("<b>"+ title.trimmed() + "</b>");
}

void GiftVisitor::textFound(const QString& text)
{
	if (!text.trimmed().isEmpty())
	{
// 		if (current == 0)
// 			current = new QStandardItem(QString(text.trimmed()));
// 		else
		current->setText(current->text()+text.trimmed());
// 		current->setText(text.trimmed());
		if (text.endsWith("\n\n"))
			closeQuestion();
	}
	else {
		qDebug() << "textFound:: vacio? " << text;
	}
}

void GiftVisitor::closeQuestion()
{
	// TODO check the question, statement, answers... 
	if ( current != 0 && !(current->text().isEmpty()) )
	{
		model->appendRow(current);
		current = 0;
	}
}

void GiftVisitor::openQuestion()
{
	qDebug() << "OpenQuestion() << new QStandardItem(" << model->rowCount()+1 << ")";
	current = new QStandardItem(QString(i18n("Question %1 ").arg(model->rowCount() + 1)));
}

void GiftVisitor::openAnswer(bool numerical)
{
	
// 	qDebug() << "openAnswer()";
// 	if (numerical)
// 		qDebug() << "numerical question";
// 	else
// 		qDebug() << "NON numerical question";
}

void GiftVisitor::closeAnswer()
{
	qDebug() << "closeAnswer()";
}

void GiftVisitor::matchAnswer(const QString& from, const QString& to, const QString& feedback)
{
// 	qDebug()<< "matchAnswer";
}

void GiftVisitor::numericalAnswer(bool interval, const QString& value, int percentage, const QString& range, const QString& feedback)
{
// 	qDebug()<< "numericalAnswer";
}
void GiftVisitor::booleanAnswer(bool answer)
{
// 	qDebug()<< "booleanAnswer";
}

void GiftVisitor::testAnswer(bool correct, int percentage, const QString& text, const QString& feedback)
{
// 	qDebug() << "testAnswer";
}

void GiftVisitor::error(const QString& error)
{}

// void GiftVisitor::titleFound(const QString& title)
// {
// 	qDebug()<< "title found: " << title;
// 	if(current)
// 		model->appendRow(current);
// 
// 	current = new QStandardItem(QString("<b>%1</b> ").arg(title));
// }
// 
// void GiftVisitor::textFound(const QString& text)
// {
// 	qDebug() << "current text: " << current->text()+ text;
// 	current->setText(current->text()+text);
// }
// 
// void GiftVisitor::openQuestion(bool numerical)
// {
// 	Q_ASSERT(currentQuestion==0);
// 
// 	currentQuestion = new QStandardItem;
// }
// 
// void GiftVisitor::closeQuestion()
// {
// 	current->appendRow(currentQuestion);
// 	currentQuestion=0;
// }
// 
// void GiftVisitor::booleanAnswer(bool answer)
// {
// 	currentQuestion->setData(AbstractImporter::Boolean, AbstractImporter::QuestionTypeRole);
// 	QStandardItem* booleanItem = new QStandardItem;
// 	booleanItem->setData(answer);
// 
// 	currentQuestion->appendRow(booleanItem);
// }
// 
// void GiftVisitor::matchAnswer(const QString& from, const QString& to, const QString& feedback)
// {
// 	currentQuestion->setData(AbstractImporter::Match, AbstractImporter::QuestionTypeRole);
// 
// 	QStandardItem* matchItem = new QStandardItem(feedback);
// 	matchItem->setData(from);
// 	matchItem->setData(to, Qt::UserRole+2);
// 
// 	currentQuestion->appendRow(matchItem);
// }
// void GiftVisitor::numericalAnswer(bool interval, const QString& value, int percentage, const QString& range, const QString& feedback)
// {
// 	currentQuestion->setData(AbstractImporter::Numerical, AbstractImporter::QuestionTypeRole);
// 
// 	QStandardItem* numericalItem = new QStandardItem(feedback);
// 	numericalItem->setData(value);
// 
// 	currentQuestion->appendRow(numericalItem);
// }
// 
// void GiftVisitor::testAnswer(bool correct, int percentage, const QString& text, const QString& feedback)
// {
// 	currentQuestion->setData(AbstractImporter::Test, AbstractImporter::QuestionTypeRole);
// 
// 	QStandardItem* testItem = new QStandardItem(feedback);
// 	testItem->setData(text);
// 	testItem->setData(correct, Qt::UserRole+2);
// 
// 	currentQuestion->appendRow(testItem);
// }
// 
// void GiftVisitor::error(const QString& error)
// {}
