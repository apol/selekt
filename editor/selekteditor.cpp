#include "abstractexporter.h"
#include "abstractimporter.h"
#include "editablemodel.h"
#include "exporterfactory.h"
#include "extract.h"
#include "importerfactory.h"
#include "selekteditor.h"
#include "listeditor.h"
#include "questionview.h"

#include <KAction>
#include <KActionCollection>
#include <KApplication>
#include <KDebug>
#include <KFileDialog>
#include <KLocale>
#include <KMessageBox>
#include <KPushButton>
#include <KStandardAction>
#include <KStatusBar>
#include <KXmlGuiWindow>

#include <QAbstractItemModel>
#include <QDockWidget>
#include <QMessageBox>
#include <QString>

SelektEditor::SelektEditor ( QWidget* parent )
	: KXmlGuiWindow ( parent )
{
	setCaption("Selekt Editor");
	setMinimumSize (900,600);

	m_questionView = new QuestionView (this);
	setCentralWidget (m_questionView);

	QDockWidget* dock = new QDockWidget (i18n ("Questions"), this);
	dock->setAllowedAreas (Qt::AllDockWidgetAreas);
	dock->setFeatures(QDockWidget::DockWidgetFloatable | QDockWidget::DockWidgetMovable);
	addDockWidget (Qt::RightDockWidgetArea, dock);

	m_listView = new ListEditor (dock);
	dock->setWidget ( m_listView );

	setMenu();

	connect ( m_listView, SIGNAL ( activated (QModelIndex, bool)), this, SLOT ( updateGUI(QModelIndex, bool)));

	isLoaded = false;
	newFile();
}

SelektEditor::~SelektEditor() {
	Extract ext(this);
	ext.removeAllExisting();
}


void SelektEditor::setMenu() {
	// File entries
	KStandardAction::openNew (this, SLOT(newFile()), actionCollection());
	KStandardAction::open (this, SLOT (loadFile()), actionCollection());
	KAction* save = KStandardAction::save (this, SLOT(saveFile()), actionCollection());
	KStandardAction::saveAs (this, SLOT(saveFileAs()), actionCollection());
	KStandardAction::quit(kapp, SLOT(quit()), actionCollection());
save->setEnabled(false);
	// Question entries
	KAction* addQuestion = new KAction(i18n("Add question"), this);
	actionCollection()->addAction("add_question", addQuestion);
	addQuestion->setIcon(KIcon("archive-insert"));
	connect(addQuestion, SIGNAL(triggered(bool)), this, SLOT(slotAddQuestion()));

	KAction* removeQuestion = new KAction(i18n("Remove question"), this);
	actionCollection()->addAction("remove_question", removeQuestion);
	removeQuestion->setIcon(KIcon("archive-remove"));
	connect(removeQuestion, SIGNAL(triggered(bool)), this, SLOT(slotRemoveQuestion()));

	KAction* addAnswer = new KAction(i18n("Add answer"), this);
	actionCollection()->addAction("add_answer", addAnswer);
	addAnswer->setIcon(KIcon("list-add"));
	connect(addAnswer, SIGNAL(triggered(bool)), this, SLOT(slotAddAnswer()));

	KAction* removeAnswer = new KAction(i18n("Remove answer"), this);
	actionCollection()->addAction("remove_answer", removeAnswer);
	removeAnswer->setIcon(KIcon("list-remove"));
	connect(removeAnswer, SIGNAL(triggered(bool)), this, SLOT(slotRemoveAnswer()));

	KAction* clearQuestion = new KAction(i18n("Clear question"), this);
	actionCollection()->addAction("clear_question", clearQuestion);
	clearQuestion->setIcon(KIcon("edit-clear"));
	connect(clearQuestion, SIGNAL(triggered(bool)), this, SLOT(clearGUI()));

	KAction* nextQuestion = new KAction(i18n("Next question"), this);
	actionCollection()->addAction("next_question", nextQuestion);
	nextQuestion->setIcon(KIcon("go-down"));
	connect(nextQuestion, SIGNAL(triggered(bool)), this, SLOT(slotNextQuestion()));

	KAction* previousQuestion = new KAction(i18n("Previous question"), this);
	actionCollection()->addAction("previous_question", previousQuestion);
	previousQuestion->setIcon(KIcon("go-up"));
	connect(previousQuestion, SIGNAL(triggered(bool)), this, SLOT(slotPreviousQuestion()));

	setupGUI(Default, "selekt-editor.rc");
}

void SelektEditor::slotNextQuestion() {
	if (isLoaded) {
		int row = actualIdx.row () + 1;
		if (model->rowCount() == row)
			row = 0;
		updateGUI(model->index(row, 0));
	}
}

void SelektEditor::slotPreviousQuestion() {
	if (isLoaded) {
		int row = actualIdx.row () - 1;
		if (row < 0)
			row = model->rowCount()-1;
		updateGUI(model->index (row, 0));
	}
}

void SelektEditor::loadFile() {
	bool isFileValid = true;
	QString extensions;
	QString allExtensions;
	QStringList supportedExtensions = ImporterFactory::self()->extensions();
	QStringList asteriskDotExtension;
	foreach ( const QString& ext, supportedExtensions)
	{
		extensions += QString ("\n*.%1|*.%1").arg (ext);
		allExtensions += QString ("*.%1 ").arg (ext);
		asteriskDotExtension << "*." + ext;
	}
	extensions += QString ("\n*.tar.gz|*.tar.gz");
	allExtensions += QString ("*.tar.gz ");

	QString location = KFileDialog::getOpenFileName (KUrl(), allExtensions + i18n("|All Supported Files") + extensions );
	if (!location.isEmpty()) {
		QFileInfo file(location);
		QString extensio = file.completeSuffix();

		if (extensio.endsWith("tar.gz")) {
			Extract ex;
			location = ex.extractFile(location, asteriskDotExtension);
			if (location.isEmpty()) {
				//Error
				qDebug()<<"There's no valid file";
				isFileValid = false;
			}
			else {
				file.setFile(location);
				extensio = file.completeSuffix();
			}
		}
		if (isFileValid) {
			if (isLoaded) {
				//TODO: mirar si el fichero ha sido modificado
				delete model;
			}
			AbstractImporter* importer = ImporterFactory::self()->get (extensio);
			QAbstractItemModel* importedModel= importer->import (location, m_listView);
			model = new EditableModel(importedModel, this);
			delete importedModel;
			m_listView->setModel (model);
			m_questionView->setModel (model);
			m_questionView->clearGUI ();
			m_questionView->loadQuestion (model->index(0,0));
			statusBar()->showMessage (i18n ("Loaded %1").arg (location), 2000);
			isLoaded = true;
			path = location;
		}
	}
}

void SelektEditor::updateGUI ( const QModelIndex& item, bool saveData) {
	actualIdx = item;
	m_questionView->loadQuestion ( item, saveData);
	m_listView->setCurrentIndex(item);
}

void SelektEditor::newFile() {
	if (isLoaded) {
		//TODO: check if exists changes
		delete model;
	}
 	QStandardItemModel* m = new QStandardItemModel(this);

	//By default, create a question with 4 possible answers
	QStandardItem* aItem = new QStandardItem(i18n("Empty statement"));
	m->appendRow(aItem);

	for (int i=0 ; i < 4 ; ++i) {
		QStandardItem* ans = new QStandardItem(i18n("Empty answer..."));
		aItem->appendRow(ans);
	}
	model = new EditableModel(m,this);
	m_listView->setModel(model);
	m_questionView->setModel(model);
	m_questionView->loadQuestion(model->index(0,0));
	isLoaded = true;
	actualIdx = model->index (0,0);
}

void SelektEditor::saveFile() {
	m_questionView->storeLastItem();

	if (path.isEmpty())
		saveFileAs();
	else {
		int p = path.lastIndexOf ('.');
		QString extension = path.mid (p+1, path.size()-p);

		AbstractExporter* exp = ExporterFactory::self()->get (extension);
		QAbstractItemModel* exportModel = model;
		exp->exporter(*exportModel, path);
	}
}

void SelektEditor::saveFileAs() {
	m_questionView->storeLastItem();
	QString extensions;
	QString allExtensions;
	foreach ( const QString& ext, ExporterFactory::self()->extensions ()) {
		extensions += QString ("\n*.%1|*.%1").arg (ext);
		allExtensions += QString ("*.%1 ").arg (ext);
	}
	path = KFileDialog::getSaveFileName (KUrl(), allExtensions + i18n("| All Supported Files") + extensions, this);
	if (!path.isEmpty()) {
		int p = path.lastIndexOf ('.');
		QString extension = path.mid (p+1, path.size()-p);

		AbstractExporter* exp = ExporterFactory::self()->get (extension);
		QAbstractItemModel* exportModel = model;
		exp->exporter(*exportModel, path);
	}
}

void SelektEditor::clearGUI() {
	m_questionView->clearQuestion();
}

void SelektEditor::slotAddAnswer() {
	model->appendAnswer (actualIdx);
	updateGUI(actualIdx, true);
}

void SelektEditor::slotRemoveAnswer() {
	model->deleteAnswer(actualIdx);
	updateGUI(actualIdx, false);
}

void SelektEditor::slotAddQuestion() {
	m_questionView->storeLastItem();
	model->appendQuestion();
}

void SelektEditor::slotRemoveQuestion() {
	model->deleteQuestion(actualIdx);
	if (model->rowCount()-1 < actualIdx.row() )
		actualIdx = model->index(model->rowCount()-1, 0);

	updateGUI(actualIdx, false);
// 	m_questionView->loadQuestion(actualIdx,false);
}
